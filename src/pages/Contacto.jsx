import {ContactForm} from '../components/ContactForm.jsx'
import React, {useRef} from 'react'


export const Contacto = ()  => {
  const ref = useRef(null);

    const login = (values) => {
        console.log(values)
    }


    return (
        <>
          <main>
            <h2>Welcome to the homepage!</h2>
            <div className="container py-3">
            <ContactForm onSubmit={login} innerRef={ref} />
            <div className="py-3"></div>
            <button className="btn btn-primary" 
            onClick={() =>{
              ref.current.submitForm();
            }}>
              Login
            </button>
            </div>
          </main>
        </>
      );
}