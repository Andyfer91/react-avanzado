import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import { Routes, Route, Link } from "react-router-dom";
import {Login} from './pages/Login.jsx'
import {Contacto} from './pages/Contacto.jsx'
import {Portada} from './pages/Portada.jsx'
import {Navbar} from './components/Navbar.jsx'
function App() {

  return (
    <div className="App">
      <h1>Bienvenido</h1>
      <div className='d-flex justify-content-left '>
      <div className="navbar-inline col-1">
      <Navbar/>
      </div>
      <div className="d-flex justify-content-center col-10">
        <Routes>
          <Route path="/" element={<Login/>} />
          <Route path="/portada" element={<Portada />} />
          <Route path="/contacto" element={<Contacto />} />
        </Routes>
        </div>

      </div>
    </div>
  )
}

export default App
