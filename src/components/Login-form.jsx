import {Field, Form, Formik} from "formik";
import background from "../assets/img/background.jpg"

export const LoginForm = ({ onSubmit, innerRef }) => {

const inicial_data = {
    user_name: '',
    password: ''
};

const submit = (values, actions) => { 
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetform();
    actions.setSubmitting(false);
};

return(
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef = {innerRef}>
        {({ isSubmiting}) => {
            return (
                <div className="card border-primary "
                style={{height:'350px'}}>
                    <img src={background} 
                    className="card-img"
                     alt="..."
                     style={{objectFit:'cover',
                     height:'222rem',
                     width:'24rem',
                     opacity:'0.8'
                     }}/>
                    <div className="card-img-overlay">
                        <Form>
                        <div className='py-2 px-5'>
                            <div className='py-1 bg-secondary text-white'>Nombre de usuario</div>
                            <label htmlFor="username"></label>
                            <Field
                            id = 'username'
                            type = 'text'
                            name = 'user_name'
                            placeholder = 'user@example.com'
                            /> 
                        </div>
                        <div className='py-2 px-5'>
                        <div className='py-1 bg-secondary text-white'>Contraseña</div>
                            <label htmlFor="Contraseña"></label>
                            <Field
                            id = 'pass'
                            type = 'password'
                            name = 'password'
                            placeholder = 'Contraseña'
                            /> 
                        </div>
                        </Form>
                    </div>
                </div>
            )
        }}
    </Formik>
)
}
export default LoginForm;