import {Link} from 'react-router-dom'

export function Navbar() {

    return (
        <nav className="navbar navbar-light bg-light">
            <div className="container-fluid ">
                <nav className="navbar-brand" href="">
                    <Link to="/">Login</Link>
                </nav>
                <nav className="navbar-brand" href="#">
                    <Link to="/portada">Portada</Link>
                </nav>
                <nav className="navbar-brand" href="#">
                    <Link to="/Contacto">Contacto</Link>
                </nav>
                <nav className="navbar-brand" href="#">
                    <Link to="/Información">Información</Link>
                </nav>
            </div>
        </nav>

    )
}
