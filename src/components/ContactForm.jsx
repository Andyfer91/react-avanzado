import {Field, Form, Formik} from "formik";
import background from "../assets/img/background.jpg"

export const ContactForm = ({ onSubmit, innerRef }) => {

const inicial_data = {
    user_name: '',
    adress: '',
    city: '',
    phone: '',
};

const submit = (values, actions) => { 
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetform();
    actions.setSubmitting(false);
};

return(
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef = {innerRef}>
        {({ isSubmiting}) => {
            return (
                <div className="card border-secondary py-4 ">
                        <Form>
                        <div className='py-2 px-5'>
                            <div className='py-1 bg-secondary text-white'>
                                chocolate
                            </div>
                            <label htmlFor="username"></label>
                            <Field
                            id = 'username'
                            type = 'text'
                            name = 'user_name'
                            placeholder = 'user@example.com'
                            /> 
                        </div>
                        <div className='py-2 px-5'>
                        <div className='py-1 bg-secondary text-white'>
                            Direccion
                        </div>
                            <label htmlFor="Contraseña"></label>
                            <Field
                            id = 'pass'
                            type = 'password'
                            name = 'password'
                            placeholder = 'Contraseña'
                            /> 
                        </div>
                        <div className='py-2 px-5'>
                            <div className='py-1 bg-secondary text-white'>
                                Ciudad
                            </div>
                            <label htmlFor="username"></label>
                            <Field
                            id = 'username'
                            type = 'text'
                            name = 'user_name'
                            placeholder = 'user@example.com'
                            /> 
                        </div>
                        <div className='py-2 px-5'>
                            <div className='py-1 bg-secondary text-white'>
                                Telefono
                            </div>
                            <label htmlFor="username"></label>
                            <Field
                            id = 'username'
                            type = 'text'
                            name = 'user_name'
                            placeholder = 'user@example.com'
                            /> 
                        </div>
                        </Form>
                </div>
            )
        }}
    </Formik>
)
}
export default ContactForm;